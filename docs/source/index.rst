.. calculs_matriciels documentation master file, created by
   sphinx-quickstart on Tue Oct  3 11:02:44 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to calculs_matriciels's documentation!
==============================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

   ./modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

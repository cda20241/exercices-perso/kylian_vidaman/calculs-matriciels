def matrice(ligne : int, col : int) : 
    """ fonction qui prend en paramètre le nombre de lignes et de colonnes de la matrice à créer.
        Elle retourne une liste à deux dimensions qui représente la matrice, toutes les valeurs valent 0 par défaut.
    Args:
        ligne (int): nombre de ligne voulu
        col (int): nombre de colonne voulu
    """
    matrice = []
    if ligne == 0 or col == 0 :
        raise ValueError("Impossible de créer une matrice idendité de taille inférieur à 1.")
    elif ligne == 1 and col == 1 :
        return [[0]]
    else : 
        for i in range(0, ligne) :
            matrice.append([])
            for j in range(0, col) :
                matrice[i].append(0)
        return matrice

def change_valeur(ligne : int, col : int, val : int, matrice : list) :
    """ fonction qui prend en paramètre la ligne et la colonne
        dans la matrice de l'élément à changer et sa nouvelle valeur, et la matrice. Elle retourne la matrice modifiée.

    Args:
        ligne (int): nombre de ligne voulu
        col (int): nombre de colonne voulu
        val (int): nouvelle valeur à intégrer à la position [ligne][col]
        matrice (list): matrice à modifier
    """
    if len(matrice) == 0 or len(matrice[0]) == 0 :
        raise ValueError()
    elif ligne >= len(matrice) or col >= len(matrice[0]) :
        raise ValueError()
    else : 
        matrice[ligne][col] = val
        return matrice

def matrice_identite(size : int) : 
    """fonction qui prend en paramètre la taille de la matrice identité. Elle crée la matrice et la retourne.
    Args:
        size (int): taille de la matrice
    """
    matrice = []
    # exception pointé avant l'execution de la partie principale
    if size < 1 :
        raise ValueError("Impossible de créer une matrice idendité de taille inférieur à 1.")
    elif size == 1 :
        return [[1]]
    else : 
        # création d'une matrice size * size
        for i in range(0, size) :
            matrice.append([])
            for j in range(0, size) :
                # si index ligne et col egual, put 1 sinon 0
                if i == j :
                    matrice[i].append(1)
                else :
                    matrice[i].append(0)
    return matrice

def print_matrice(matrice : list):
    """fonction print_matrice() qui prend en paramètre la matrice et l'affiche dans la console.
    Args:
        matrice (list): matrice
    """
    # on enlève les exceptions
    if len(matrice) == 0 :
        raise ValueError("Impossible d'afficher une matrice null.")
    else :
        txt = ""
        for i in range(0, len(matrice)) :
            txt += "\n"
            for j in range(0, len(matrice[0])):
                txt = txt + " " + str(matrice[i][j])
        print(txt)

def dimension_check(matrice : list, alt_matrice: list) :
    """ fonction qui prend en paramètre 2 matrices 
        et qui retourne True si elles sont de même dimension, False sinon
    Args:
        matrice (list): une matrice
        alt_matrice (list): une autre matrice
    Returns:
        [boolean]: return true si même dimension et false sinon
    """
    if len(matrice) == 0 or len(alt_matrice) == 0 :
        raise ValueError("Impossible de comparer si l'une des matrices est null")
    elif len(matrice) >= 1 and len(alt_matrice) >= 1 : 
        if (len(matrice) == len(alt_matrice) != None) and (len(matrice[0]) == len(alt_matrice[0]) != None) :
            return True
        else : return False
    else : return False 

def somme_matrice(matrice : list, alt_matrice : list) : 
    """ fonction somme_matrice() qui prend en paramètre 2 matrices et en retourne la somme.
     Cette opération n'est possible que si les 2 matrices sont de même dimension, si ce n'est pas le cas lever une exception de type MatriceDimensionError().
    Args:
        matrice (list): une matrice
        alt_matrice (list): une autre matrice
    Returns:
        new_matrice (list): la somme des 2 matrices en args
    """
    if dimension_check(matrice, alt_matrice) != True :
        raise ValueError("Impossible de fusionner 2 matrices si ells ne possède pas la même dimensions")
    elif dimension_check(matrice, alt_matrice) :
        new_matrice = []
        # parcourt les 2 matrices
        for i in range(0, len(matrice)) :
            new_matrice.append([])
            for j in range(0, len(matrice[0])) :
                new_matrice[i].append(matrice[i][j] + alt_matrice[i][j])
        return new_matrice

def is_element(matrice : list, int : int) : 
    """fonction is_element() qui prend en paramètre une matrice et un nombre, 
        et vérifie si ce nombre est dans la matrice. Si c'est le cas on retourne sa position x, y.
    Args:
        matrice (list): une matrice
        int (int): un nombre
    Returns:
        liste : une liste avec coordonée (x, y)
    """
    if len(matrice) == 0 :
        raise ValueError("Impossible")
    elif len(matrice) == 1 and len(matrice[0]) == 1:
        if matrice[0][0] == int :
            return (0,0)
    else : 
        for i in range(0, len(matrice)) : 
            for j in range(0, len(matrice)):
                if matrice[i][j] == int: 
                    return (i,j)
    return None

def produit_scalaire(matrice: list, mutl : int or float) :
    """fonction produit_scalaire(), qui prend en paramètre une matrice
     et un nombre entier ou décimal, et en retourne le produit scalaire.
    Args:
        matrice (list): une matrice
        mutl (intorfloat): produit scalaire
    Returns:
        list: matrice modifié par le produit scalaire
    """
    if len(matrice) == 0 or len(matrice[0]) == 0  :
        raise ValueError("Impossible")
    elif len(matrice) == 1 and len(matrice[0]) == 1:
        matrice[0][0] *= mutl
        return matrice
    else :
        for i in range(0, len(matrice)) : 
            for j in range(0, len(matrice)):
                matrice[i][j] *= mutl
        return matrice

def produit_matriciel(matrice : list, alt_matrice : list) :
    """fonction produit_matriciel() qui prend en paramètre 2 matrices, et en retourne le produit matriciel.
     Vérifier si les dimensions des 2 matrices sont cohérentes pour le produit matriciel,
     si ce n'est pas le cas lever une exception de type MatriceDimensionError().
    Args:
        matrice (list): une matrice 
        alt_matrice (list): une autre matrice
    Returns:
        list: une nouvelle matrice du produit matriciel des 2 matrices arguments
    """
    if dimension_check(matrice, alt_matrice) != True :
        raise ValueError("Impossible de fusionner 2 matrices si ells ne possède pas la même dimensions")
    elif dimension_check(matrice, alt_matrice) :
        new_matrice = []
        # parcourt les 2 matrices
        for i in range(0, len(matrice)) :
            new_matrice.append([])
            for j in range(0, len(matrice[0])) :
                new_matrice[i].append(matrice[i][j] * alt_matrice[i][j])
        return new_matrice

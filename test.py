import unittest
import matrice as m

# test function du fichier matrice.py
class TestMatrice(unittest.TestCase) :
    def test_matrice(self) :
        self.assertEqual(m.matrice(3,3),[[0,0,0],[0,0,0],[0,0,0]])
        with self.assertRaises(ValueError): m.matrice(0,0)
        self.assertEqual(m.matrice(1,1),[[0]])
        self.assertEqual(m.matrice(2,2),[[0,0],[0,0]])

    def test_change_valeur(self) :
        self.assertEqual(m.change_valeur(0,0,1,m.matrice(3,3)), [[1,0,0],[0,0,0],[0,0,0]])
        self.assertEqual(m.change_valeur(1,1,1,m.matrice(3,3)), [[0,0,0],[0,1,0],[0,0,0]])
        self.assertEqual(m.change_valeur(2,2,1,m.matrice(3,3)), [[0,0,0],[0,0,0],[0,0,1]])
        with self.assertRaises(ValueError) : m.change_valeur(0,0,1,[])
        with self.assertRaises(ValueError) : m.change_valeur(2,2,1,[[0]])

    def test_matrice_identite(self) :
        self.assertEqual(m.matrice_identite(1), [[1]])
        self.assertEqual(m.matrice_identite(2), [[1,0],[0,1]])
        self.assertEqual(m.matrice_identite(3), [[1,0,0],[0,1,0],[0,0,1]])
        with self.assertRaises(ValueError) : m.matrice_identite(0)
    
    def test_print_matrice(self) :
        self.assertEqual(m.print_matrice(m.matrice_identite(2)), None)
        with self.assertRaises(ValueError) : m.print_matrice([])

    def test_dimension_check(self) :
        with self.assertRaises(ValueError) : m.dimension_check([], [])
        self.assertTrue(m.dimension_check([[1]],[[0]]))
        self.assertFalse(m.dimension_check([[1,0]],[[0]]))

    def test_somme_matrice(self) : 
        with self.assertRaises(ValueError) : m.somme_matrice([], [])
        with self.assertRaises(ValueError) : m.somme_matrice([[1]], [[0,1]])
        with self.assertRaises(ValueError) : m.somme_matrice([[1], [1]], [[0]])
        self.assertEqual(m.somme_matrice([[1]], [[1]]), [[2]])
        self.assertEqual(m.somme_matrice([[1,1],[1,1]], [[1,1],[1,1]]), [[2,2],[2,2]])

    def test_is_element(self) :
        with self.assertRaises(ValueError) : m.is_element([], 1)
        self.assertEqual(m.is_element([[1]], 1), (0,0))
        self.assertEqual(m.is_element([[1,0],[0,2]], 2), (1,1))
        self.assertEqual(m.is_element([[1,0],[0,1]], 2), None)

    def test_produit_scalaire(self) :
        with self.assertRaises(ValueError) : m.produit_scalaire([[]], 2)
        self.assertEqual(m.produit_scalaire([[2]], 2), [[4]])
        self.assertEqual(m.produit_scalaire([[2,2],[2,2]], 2), [[4,4],[4,4]])

    def test_produit_matriciel(self) :
        with self.assertRaises(ValueError) : m.produit_matriciel([[1],[1]],[[1]])
        self.assertEqual(m.produit_matriciel([[2,2],[2,2]],[[2,2],[2,2]]),[[4,4],[4,4]])

if __name__ == '__main__' :
    unittest.main()
    